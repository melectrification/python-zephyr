# ME's Python Zephyr Library #

This module is meant to be used as a simple library for using Zephyr Scale API functions.

### What is this repository for? ###

* Generic abstractions of common API calls, e.g.
  * Get a list of all projects
  * Get project, folder or user IDs
  * Create tests or test executions
  * ...

### How do I get set up? ###

1. Install specified requirements:
````commandline
pip install -r requirements.txt
````
2. Install pre-commit hooks:
````commandline
pre-commit install
````
3. Get familiar with the API: https://support.smartbear.com/zephyr-scale-cloud/api-docs/

### Contribution guidelines ###

* Fix rather than suppress any pre-commit warnings
* Try to stick to existing coding patterns and use/extend given structures
